# -*- coding: utf-8 -*-
"""
Created on Tue May 19 19:48:49 2020

@author: 109457
"""
import tensorflow as tf
from tensorflow.keras import layers
import numpy as np
import os
import sklearn
from sklearn import datasets
import numpy as np
from sklearn import decomposition
import scipy
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Dense, Layer, InputSpec
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
from tensorflow.keras import regularizers, activations, initializers, constraints, Sequential
from tensorflow.keras import backend as K
from tensorflow.keras.constraints import UnitNorm, Constraint


class Linear_encoder(tf.keras.layers.Layer):
  def __init__(self, layer_output_dim, filename, **kwargs):
    super(Linear_encoder, self).__init__()
    self.layer_output_dim = layer_output_dim
    self.filename = filename
  def build(self, input_shape):
      # Wenc = np.array([[0.5361779 , 0.10770221],
      #                     [ 0.4954218 , -0.53083354 ],
      #                     [-0.50844705 , 0.30159527], 
      #                     [ 0.4570274 ,  0.78462416 ]],dtype="float32") #For MEXICO
      PCs =  np.array(np.load(os.path.join("Output","PCs"+ str(self.filename)+ ".npy")),dtype = "float32")
      Wenc = np.transpose(np.array(PCs))
      # Wenc = np.array([[-0.3183008 ,  0.1584541 ],
      #                     [-0.2189381 , -0.32958108],
      #                     [-0.22694632,  0.20797946],
      #                     [-0.32516378,  0.03471973],
      #                     [ 0.2540281 ,  0.11403453],
      #                     [ 0.3379938 , -0.10989073],
      #                     [ 0.09213914,  0.32553494],
      #                     [-0.2350233 ,  0.20876971],
      #                     [-0.19147654, -0.3406452 ],
      #                     [-0.06587078, -0.38125962],
      #                     [-0.03261106, -0.37347838],
      #                     [-0.09137639, -0.31907773],
      #                     [-0.12755667, -0.28463304],
      #                     [ 0.07120586, -0.32739577],
      #                     [ 0.03268503, -0.35138008],
      #                     [-0.29302928,  0.04311191],
      #                     [-0.29825523,  0.04572382],
      #                     [-0.33027005,  0.08791498],
      #                     [-0.32020786,  0.0545646 ]], dtype="float32") #For PORTO
      
      self.w = tf.Variable(
            initial_value=Wenc,
            trainable=False,
        )
      
  def call(self, inputs_layer):
    print()
    return tf.matmul(inputs_layer, self.w)



class MyDenseLayer(tf.keras.layers.Layer):
  def __init__(self, layer_output_dim, **kwargs):
    super(MyDenseLayer, self).__init__()
    self.layer_output_dim = layer_output_dim

  def build(self, input_shape):
    self.w = self.add_weight("weights", trainable=True, shape=[int(input_shape[-1]), self.layer_output_dim])
    self.b = self.add_weight("bias", trainable=True, shape=[self.layer_output_dim])

  def call(self, inputs_layer):
    print()
    return tf.matmul(inputs_layer, self.w) + self.b, self.w


class Linear_decoder(tf.keras.layers.Layer):
  def __init__(self, layer_output_dim, filename, **kwargs):
    super(Linear_decoder, self).__init__()
    self.layer_output_dim = layer_output_dim
    self.filename = filename

  def build(self, input_shape):
      # Wenc = np.array([[0.5361779 , 0.10770221],
      #                     [ 0.4954218 , -0.53083354 ],
      #                     [-0.50844705 , 0.30159527], 
      #                     [ 0.4570274 ,  0.78462416 ]],dtype="float32") #For MEXICO
      PCs = np.array(np.load(os.path.join("Output","PCs"+ str(self.filename)+ ".npy")),dtype = "float32")
      Wdec = np.array(PCs)
      # Wenc = np.array([[-0.30596218,  0.0356522 ],
      #                     [-0.30610746, -0.03440775],
      #                     [-0.3068664 , -0.02674916],
      #                     [-0.30760762, -0.01269176],
      #                     [ 0.2826412 ,  0.06621638],
      #                     [ 0.29164115, -0.00403261],
      #                     [ 0.07595379,  0.32227644],
      #                     [-0.24360988,  0.20094454],
      #                     [-0.19593804, -0.27707368],
      #                     [-0.01085131, -0.38168094],
      #                     [-0.01925445, -0.380688  ],
      #                     [-0.09074109, -0.339543  ],
      #                     [-0.1234861 , -0.32474968],
      #                     [ 0.0515663 , -0.34212235],
      #                     [ 0.01377769, -0.34273276],
      #                     [-0.297788  ,  0.06715937],
      #                     [-0.2945903 ,  0.1115348 ],
      #                     [-0.2939566 ,  0.09676074],
      #                     [-0.29465437,  0.11137693]], dtype="float32") #For PORTO
      # Wdec = Wenc.T
      self.w = tf.Variable(
            initial_value=Wdec,
            trainable=False,
        )
      
  def call(self, inputs_layer):
    print()
    return tf.matmul(inputs_layer, self.w)    

# class Linear_decoder(layers.Layer):
#     def __init__(self,encoder_weights,**kwargs):
#         super(Linear_decoder, self).__init__()
#         self.encoder_weights = encoder_weights
#     def build(self, input_shape):
        
#         self.w = self.add_weight("weights", trainable=False, shape=[2, 4])
    
    
#     def call(self, inputs):
#         # self.w = K.transpose(self.encoder_weights)
#         tf.print('******************************************')
#         tf.print(self.w)
#         tf.print('******************************************')
#         tf.print(self.encoder_weights)
#         # tf.print(self.Linear_encoder.__dict__) 
#         # tf.print(self.Linear_encoder._id) #extraer de la lista de dict 
        
#         return tf.matmul(inputs, self.w)
#         # return inputs


def architecture_PCA_encoder(input_dim, enc_dim, filename):
    input1 = tf.keras.Input(shape =(input_dim,), name = 'input_layer'+str(filename))
    output1 = layers.Dense(enc_dim, activation = 'linear', use_bias=True, name = 'output1'+str(filename))(input1)
    # output1 =  Linear_encoder(enc_dim, filename, name = 'linear_encoder')(input1)
    
    return input1, output1


def architecture_PCA_decoder(input_dim, enc_dim, filename):
    input2 = tf.keras.Input(shape =(enc_dim,), name = 'decoder_input')
    output2 =tf.keras.layers.Dense(input_dim,activation = 'linear', use_bias=True, name = 'output2'+str(filename))(input2)
    # output2 = Linear_decoder(input_dim, filename, name = 'linear_dec')(input2)
    # output2 = DenseTied(input_dim, activation="linear", tied_to=Linear_encoder, use_bias = False)(input2)
     
    return input2, output2

def architecture_residual_encoder_Porto(input_dim, enc_dim,filename):
    input3 = tf.keras.Input(shape =(input_dim,), name = 'residual_input'+str(filename))
    lay1 = layers.Dense(32, activation = 'relu', name = 'H1'+str(filename))(input3) #Intermediate layers
    lay2 = layers.Dense(36, activation = 'relu', name = 'H2'+str(filename))(lay1) #Intermediate layers
    lay3 = layers.Dense(48, activation = 'relu', name = 'H3'+str(filename))(lay2) #Intermediate layers
    lay4 = layers.Dense(36, activation = 'relu')(lay3) #Intermediate layers
    lay5 = layers.Dense(48, activation = 'relu')(lay4) #Intermediate layers
    lay6 = layers.Dense(24, activation = 'relu')(lay5) #Intermediate layers
    output3 =tf.keras.layers.Dense(enc_dim,activation = 'linear' , name = 'output3'+str(filename))(lay6)
    return input3, output3


def architecture_residual_decoder_Porto(input_dim, enc_dim,filename):
    input4 = tf.keras.Input(shape =(enc_dim,), name = 'residual_input'+str(filename))
    lay1 = layers.Dense(24, activation = 'relu')(input4) #Intermediate layers
    lay2 = layers.Dense(48, activation = 'relu')(lay1) #Intermediate layers
    lay3 = layers.Dense(36, activation = 'relu')(lay2) #Intermediate layers
    lay4 = layers.Dense(48, activation = 'relu')(lay3) #Intermediate layers
    lay5 = layers.Dense(36, activation = 'relu')(lay4) #Intermediate layers
    lay6 = layers.Dense(32, activation = 'relu')(lay5) #Intermediate layers
    output4 =tf.keras.layers.Dense(input_dim,activation = 'linear', name = 'output4'+str(filename))(lay6)
    return input4, output4
#################################################################################################################

def architecture_residual_encoder_Porto_Combi(input_dim, enc_dim,filename):
    input3 = tf.keras.Input(shape =(input_dim,), name = 'residual_input'+str(filename))
    lay1 = layers.Dense(64, activation = 'relu', name = 'H1'+str(filename))(input3) #Intermediate layers
    lay2 = layers.Dense(72, activation = 'relu', name = 'H2'+str(filename))(lay1) #Intermediate layers
    lay3 = layers.Dense(96, activation = 'relu', name = 'H3'+str(filename))(lay2) #Intermediate layers
    lay4 = layers.Dense(72, activation = 'relu')(lay3) #Intermediate layers
    lay5 = layers.Dense(96, activation = 'relu')(lay4) #Intermediate layers
    lay6 = layers.Dense(48, activation = 'relu')(lay5) #Intermediate layers
    output3 =tf.keras.layers.Dense(enc_dim,activation = 'linear' , name = 'output3'+str(filename))(lay6)
    return input3, output3


def architecture_residual_decoder_Porto_Combi(input_dim, enc_dim,filename):
    input4 = tf.keras.Input(shape =(enc_dim,), name = 'residual_input'+str(filename))
    lay1 = layers.Dense(48, activation = 'relu')(input4) #Intermediate layers
    lay2 = layers.Dense(96, activation = 'relu')(lay1) #Intermediate layers
    lay3 = layers.Dense(72, activation = 'relu')(lay2) #Intermediate layers
    lay4 = layers.Dense(96, activation = 'relu')(lay3) #Intermediate layers
    lay5 = layers.Dense(72, activation = 'relu')(lay4) #Intermediate layers
    lay6 = layers.Dense(64, activation = 'relu')(lay5) #Intermediate layers
    output4 =tf.keras.layers.Dense(input_dim,activation = 'linear', name = 'output4'+str(filename))(lay6)
    return input4, output4





#####################################################################################################3

def architecture_Residual_encoder_Mexico(input_dim, enc_dim):
    input3 = tf.keras.Input(shape =(input_dim,), name = 'residual_input')
    lay1 = layers.Dense(8, activation = 'relu')(input3) #Intermediate layers
    lay2 = layers.Dense(12, activation = 'relu')(lay1) #Intermediate layers
    lay3 = layers.Dense(16, activation = 'relu')(lay2) #Intermediate layers
    output3 =tf.keras.layers.Dense(enc_dim,activation = 'linear' , name = 'output3')(lay3)
    return input3, output3


def architecture_Residual_decoder_Mexico(input_dim, enc_dim):
    input4 = tf.keras.Input(shape =(enc_dim,), name = 'residual_input')
    lay1 = layers.Dense(16, activation = 'relu')(input4) #Intermediate layers
    lay2 = layers.Dense(12, activation = 'relu')(lay1) #Intermediate layers
    lay3 = layers.Dense(8, activation = 'relu')(lay2) #Intermediate layers
    output4 =tf.keras.layers.Dense(input_dim,activation = 'linear', name = 'output4')(lay3)
    return input4,output4

#####################################################################################################

#function para que me devuelva diccionario de arquitecturas
def arch_dictionary():
    #Definir diccionariode arquitecturas
    architecture_dictionary = {
        "arch_Residual_encoder_Mexico" : architecture_Residual_encoder_Mexico,
        "arch_Residual_decoder_Mexico" : architecture_Residual_decoder_Mexico,
        "arch_Residual_encoder_Porto" : architecture_residual_encoder_Porto,
        "arch_Residual_decoder_Porto" : architecture_residual_decoder_Porto,
        "arch_Residual_encoder_Porto_comb" : architecture_residual_encoder_Porto_Combi,
        "arch_Residual_decoder_Porto_comb" : architecture_residual_decoder_Porto_Combi,
        "arch_PCA_encoder" :  architecture_PCA_encoder,
        "arch_PCA_decoder" :  architecture_PCA_decoder     
        }
    return architecture_dictionary


