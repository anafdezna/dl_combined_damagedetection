# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 09:35:02 2022

@author: 110137
"""

# PROBAR ADAM OPTIMIZER* EN LUGAR DE RMS PROP, ACTIVAR CALLBACK PARA RETENER MEJOR SOLUCIÓN (Y NO LA ÚLTIMA) EN BASE A VAL_LOSS
# APRENDER A METER BATCH VARIABLE
# DOMINAR EL SUBCLASSING PARA CREAR MODELOS Y CUSTOM LAYERS

import tensorflow as tf
from MODULES.MODEL.submodels_creation import architecture_info_initializer
from MODULES.MODEL.training import training_info_initializer, building_models, training_models, custom_loss
from MODULES.PREPROCESSING.preprocessing import preprocessing_interface
from MODULES.POSTPROCESSING.postprocessing import postprocessing_info_initializer, postprocessing_interface

tf.keras.backend.clear_session()
tf.random.set_seed(4321)

#configure to run_functions in eagerly or not True only recomended for debug
# tf.config.run_functions_eagerly(True)

filename = "Pf5_global"
#########################################################################################################################################
#INITIAL CONSIDERATIONS

#Choose the model type to solve
Residual = True
PCA = False

model_type = "Residual"
# model_type = "Linear"

#Choose the bridge location 
# bridge_loc = "MEXICO"
bridge_loc = "PORTO"
enc_dim = 3
#Load data and preprocessing
Xtrain_std, Xval_std, Xtest_std, Xtest_dam_std = preprocessing_interface(filename,bridge_loc,enc_dim)

#Initialize architecture properties and select the architectures FROM the DICTIONARY
architecture_info = architecture_info_initializer(
                        PCA_enc = "arch_PCA_encoder", 
                        PCA_dec = "arch_PCA_decoder", 
                        Res_enc = "arch_Residual_encoder_Porto", 
                        Res_dec = "arch_Residual_decoder_Porto", 
                        input_dim = Xtrain_std.shape[1], 
                        # input_dim = 14,
                        enc_dim = enc_dim,
                        filename = filename)

####################################################################################################################################

#BUILD AND TRAIN THE MODELS
#Initiailize the training properties
training_info = training_info_initializer(
                n_epoch = 800, 
                batch_size = 1024, 
                LR = 1e-03, 
                metrics = ["accuracy"], 
                loss = custom_loss, 
                shuffle = True)

models, model_parts = building_models(Residual, PCA, architecture_info, training_info, filename)
Autoencoder = models[1]
Encoder = models[2]

#TRAINING:
history = training_models(Autoencoder, training_info, Xtrain_std,Xval_std, filename)
from MODULES.POSTPROCESSING.postprocessing_tools import plot_loss_evolution
plot_loss_evolution(history,filename)
#WE WANT TO ANALYZE THE LATENT SPACE Z (ENCODER OUTPUT) 
#FIRST CASE EXAMPLE IS DAMAGE OF 10% AT M1 (D1_10)
Xtrain_std, Xval_std, Xtest_std, Xtest_dam_std = preprocessing_interface(filename,bridge_loc,enc_dim)
filename = 'D2_10_combined'
Z_train = Encoder.predict(Xtrain_std)
Z_val = Encoder.predict(Xval_std)
Z_test = Encoder.predict(Xtest_std)
Z_test_dam = Encoder.predict(Xtest_dam_std)
from MODULES.POSTPROCESSING.postprocessing_tools import compress_Z_space
from MODULES.POSTPROCESSING.postprocessing_tools import plot_latent_vars, plot_latent_vars3
plot_latent_vars(Z_test, Z_test_dam, filename)
# plot_latent_vars3(Z_test, Z_test_dam, filename)



#POSTPROCESSING
#EVALUATE EXPERIMENTAL DAMAGE SCENARIOS
# Initialize postprocessing properties
postprocessing_info = postprocessing_info_initializer(
                k = 5, #Groups of k measurements for the CONTROLCHARTS. In case I need cummulative reconstruction error (Mexico k = 6 to obtain 1measurement/hour)
                dam_level = 0.1, #Used to plot the level of applied damage (Example -> 0.2 -> 20%)
                percentile  = 97)

#Obtain results
# import numpy as np
# from MODULES.POSTPROCESSING.postprocessing_tools import calculate_ROC_metrics, plot_ROCs, plot_ROCs_b
# import os
# Combined_Model = tf.keras.models.load_model(os.path.join("Output","best_modelPf5_Combined.hdf5"),custom_objects={"custom_loss":custom_loss})

Train_rec_error, Val_rec_error, Test_rec_error, Test_rec_error_dam, Test_rec_errors, Lim, individual_errors, TPs, FPs = postprocessing_interface(PCA,Residual,models, Xtrain_std, Xval_std, Xtest_std, Xtest_dam_std, postprocessing_info)
Test_rec_errorD4 = Test_rec_error_dam
# print(individual_errors[30,12:26])
# Err  = Test_rec_error_dam[30]
# print(np.sum(individual_errors[30,12:14])/2*100/(Err*26))
# print(np.sum(individual_errors[30,14:16])/2*100/(Err*26))
# print(np.sum(individual_errors[30,16:18])/2*100/(Err*26))
# print(individual_errors[30,18]*100/(Err*26))
# print(np.sum(individual_errors[30,19:22])/3*100/(Err*26))
# print(np.sum(individual_errors[30,22:26])/4*100/(Err*26))

from MODULES.POSTPROCESSING.postprocessing_tools import calculate_ROC_metrics, plot_ROCs, plot_ROCs_b
import os
import numpy as np 

FP_rates, TP_rates, Lims = calculate_ROC_metrics(Test_rec_error, Test_rec_error_dam, Train_rec_error,filename)

FP_path = os.path.join("Output","FP_rates_"+str(filename)+".npy")
np.save(FP_path, FP_rates)
TP_path = os.path.join("Output","TP_rates_"+str(filename)+".npy")
np.save(TP_path, TP_rates)

Test_rec_errorD4 = Test_rec_error_dam

from MODULES.POSTPROCESSING.postprocessing_tools import plot_outliers_dam2
dam_level = 0.1
percentile = 97
plot_outliers_dam2(Train_rec_error,Test_rec_error, Test_rec_errorD1,Test_rec_errorD2,Test_rec_errorD3,Test_rec_errorD4, dam_level, percentile)







# fig = plt.figure(figsize=(4,4))
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(Z_test[:,0],Z_test[:,2],Z_test[:,1], s = 1.5, color  = 'green') # plot the point (2,3,4) on the figure
# ax.scatter(Z_test_dam[:,0],Z_test_dam[:,2],Z_test_dam[:,1], s = 1.5, color  = 'red') # plot the point (2,3,4) on the figure
# fig.savefig(os.path.join("Figures","3DLatentb"+ filename +".png"), dpi = 500, bbox_inches='tight')
# plt.show()



# Zspace_plot = plt.figure()
# # plt.plot(Z_pca_train[:,0],Z_pca_train[:,1], 'o', markersize = 1, color = 'blue')
# plt.plot(Z_test[:,0],Z_test[:,1], 'o', markersize = 1, color = 'green')
# plt.plot(Z_test_dam[:,0],Z_test_dam[:,1], 'o', markersize = 1, color = 'red')
# plt.show()


# x = np.linspace(1,Z_test.shape[0],Z_test.shape[0])
# plt.plot(x,Z_test, color = 'green')
# plt.plot(x,Z_test_dam, color  = 'red')
# plt.savefig(os.path.join("Figures","8DLatent"+ filename +".png"), dpi = 500, bbox_inches='tight')


# relative_error =np.abs(Z_test - Z_test_dam)/Z_test_dam
# rel_err = relative_error<1
# plt.plot(rel_err)


import numpy as np 
import os 
path = os.path.join("Data","Rot_D1_20_10second.npy")
Data = np.load(path)
time = Data[:,0]
Und_signal = Data[:,2]
Dam_signal20 = Data[:,1]
from matplotlib import pyplot as plt

Fig_signal = plt.figure(figsize = (6.5,4.5))
# plt.plot(time, Dam_signal20, color = 'red',  linewidth = 1, marker = 's', markersize = 1.5)
plt.plot(time, Dam_signal20, color = 'red',  linewidth = 1, marker = 's', markersize = 1.5)
plt.plot(time,Und_signal, color = 'blue', linewidth = 1, marker = '^', markersize = 1.5)
plt.xlabel('Time [s]')
plt.ylabel('Inclination [rad]')
plt.legend(['Damaged', 'Undamaged'], loc='upper right', fontsize = 16)
plt.savefig(os.path.join("Figures",'Rot10seconds_D1_20sev'),dpi = 500)
plt.show()























# Z_pca_train = compress_Z_space(Z_train)
# Z_pca_test = compress_Z_space(Z_test)
# Z_pca_test_Dam = compress_Z_space(Z_test_dam)

# Zspace_plot = plt.figure()
# # plt.plot(Z_pca_train[:,0],Z_pca_train[:,1], 'o', markersize = 1, color = 'blue')
# plt.plot(Z_pca_test[:,0],Z_pca_test[:,1], 'o', markersize = 1, color = 'green')
# plt.plot(Z_pca_test_Dam[:,0],Z_pca_test_Dam[:,1], 'o', markersize = 1, color = 'red')
# plt.show()




##########################################################################################################

#POSTPROCESSING
#EVALUATE EXPERIMENTAL DAMAGE SCENARIOS
# Initialize postprocessing properties
postprocessing_info = postprocessing_info_initializer(
                k = 5, #Groups of k measurements for the CONTROLCHARTS. In case I need cummulative reconstruction error (Mexico k = 6 to obtain 1measurement/hour)
                dam_level = 0.1, #Used to plot the level of applied damage (Example -> 0.2 -> 20%)
                percentile  = 96)

#Obtain results
import numpy as np
Train_rec_error, Val_rec_error, Test_rec_error, Test_rec_error_dam, Test_rec_errors, Lim, individual_errors, TPs, FPs = postprocessing_interface(PCA,Residual,models, Xtrain_std, Xval_std, Xtest_std, Xtest_dam_std, postprocessing_info)
# print(individual_errors[30,12:26])
# Err  = Test_rec_error_dam[30]
# print(np.sum(individual_errors[30,12:14])/2*100/(Err*26))
# print(np.sum(individual_errors[30,14:16])/2*100/(Err*26))
# print(np.sum(individual_errors[30,16:18])/2*100/(Err*26))
# print(individual_errors[30,18]*100/(Err*26))
# print(np.sum(individual_errors[30,19:22])/3*100/(Err*26))
# print(np.sum(individual_errors[30,22:26])/4*100/(Err*26))

# from MODULES.POSTPROCESSING.postprocessing_tools import calculate_ROC_metrics, plot_ROCs, plot_ROCs_b
# import os
# import numpy as np 

# FP_rates, TP_rates, Lims = calculate_ROC_metrics(Test_rec_error, Test_rec_error_dam, Train_rec_error,filename)

# FP_path = os.path.join("Output","FP_rates_"+str(filename)+".npy")
# np.save(FP_path, FP_rates)
# TP_path = os.path.join("Output","TP_rates_"+str(filename)+".npy")
# np.save(TP_path, TP_rates)

# Test_rec_errorD4 = Test_rec_error_dam

#######################################################################################################
# # Once the three sensor combinations have been considered, we plot the ROC curves:
filename = "Pf4F4_D2_20"

    
FP_rates_local = np.load("Output/FP_rates_Pf4_LocalF4_D2_20.npy")
TP_rates_local = np.load("Output/TP_rates_Pf4_LocalF4_D2_20.npy")
FP_rates_global = np.load("Output/FP_rates_Pf4_GlobalF4_D2_20.npy")
TP_rates_global = np.load("Output/TP_rates_Pf4_GLobalF4_D2_20.npy")
FP_rates_combined = np.load("Output/FP_rates_Pf4_CombinedF4_D2_20.npy")
TP_rates_combined = np.load("Output/TP_rates_Pf4_CombinedF4_D2_20.npy")

# plot_ROCs(FP_rates_global, FP_rates_local,FP_rates_combined, TP_rates_global, TP_rates_local, TP_rates_combined,filename)

# filename  = 'GlobalF4_S_20'
# FP_rates_D1 = np.load("Output/FP_rates_Pf4_GlobalF4_D1_20.npy")
# TP_rates_D1 = np.load("Output/TP_rates_Pf4_GlobalF4_D1_20.npy")
# FP_rates_D2 = np.load("Output/FP_rates_Pf4_GlobalF4_D2_20.npy")
# TP_rates_D2 = np.load("Output/TP_rates_Pf4_GlobalF4_D2_20.npy")
# FP_rates_D3 = np.load("Output/FP_rates_Pf4_GlobalF4_D3_20.npy")
# TP_rates_D3 = np.load("Output/TP_rates_Pf4_GlobalF4_D3_20.npy")
# FP_rates_D4 = np.load("Output/FP_rates_Pf4_GlobalF4_D4_20.npy")
# TP_rates_D4 = np.load("Output/TP_rates_Pf4_GlobalF4_D4_20.npy")
# # plot_ROCs_b(FP_rates_D1, FP_rates_D2, FP_rates_D3, FP_rates_D4, TP_rates_D1, TP_rates_D2, TP_rates_D3, TP_rates_D4, filename)
# from MODULES.POSTPROCESSING.postprocessing_tools import plot_outliers_dam2
# dam_level = 0.1
# percentile = 97
# # plot_outliers_dam2(Train_rec_error,Test_rec_error, Test_rec_errorD1,Test_rec_errorD2,Test_rec_errorD3,Test_rec_errorD4, dam_level, percentile)

