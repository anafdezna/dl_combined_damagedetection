# -*- coding: utf-8 -*-
"""
Created on Wed May 20 08:45:54 2020

@author: 109457
"""
import tensorflow as tf
#Create the autoencoder from the two Submodels (encoder, decoder)

def PCA_model_creation(input_layer, linear_encoder, linear_decoder):
    output_encoder = linear_encoder(input_layer)
    output_decoder = linear_decoder(output_encoder)
    modelPCA_autoencoder = tf.keras.Model(inputs = input_layer, outputs = output_decoder, name = 'autoencoder')
    return modelPCA_autoencoder

####################################################################################
# def ResNet_model_creation(input_layer, linear_encoder, linear_decoder, nonlinear_encoder, nonlinear_decoder):
#     output_linear_encoder = linear_encoder(input_layer)
#     output_nonlinear_encoder = nonlinear_encoder(input_layer)
#     output_encoder = output_linear_encoder + output_nonlinear_encoder
#     output_linear_decoder = linear_decoder(output_encoder)
#     output_nonlinear_decoder = nonlinear_decoder(output_encoder)
#     output_autoencoder = output_linear_decoder + output_nonlinear_decoder
#     modelResNet_autoencoder = tf.keras.Model(inputs = input_layer, outputs = output_autoencoder, name = 'autoencoder2')
#     return modelResNet_autoencoder

#CREATED FOR SHM REVIEW TO OUTPUT ALSO THE ENCODER MODEL
def ResNet_model_creation(input_layer, linear_encoder, linear_decoder, nonlinear_encoder, nonlinear_decoder):
    output_linear_encoder = linear_encoder(input_layer)
    output_nonlinear_encoder = nonlinear_encoder(input_layer)
    modelResNet_encoder = tf.keras.Model(inputs = input_layer, outputs = output_linear_encoder + output_nonlinear_encoder, name = 'Encoder')
    output_encoder = modelResNet_encoder(input_layer)
    output_linear_decoder = linear_decoder(output_encoder)
    output_nonlinear_decoder = nonlinear_decoder(output_encoder)
    output_autoencoder = output_linear_decoder + output_nonlinear_decoder
    modelResNet_autoencoder = tf.keras.Model(inputs = input_layer, outputs = output_autoencoder, name = 'autoencoder2')
    return modelResNet_autoencoder, modelResNet_encoder

##########################################################################################################
#This model is not required in the end
# def Combined_model_creation(input_layer, linear_encoder1, linear_encoder2, nonlinear_encoder1, nonlinear_encoder2, linear_decoder, nonlinear_decoder):
#     input_layer1, input_layer2 = tf.split(input_layer,[12,14],1)
#     output_linear_encoder1 = linear_encoder1(input_layer1)
#     output_linear_encoder2 = linear_encoder2(input_layer2)
#     output_nonlinear_encoder1 = nonlinear_encoder1(input_layer1)
#     output_nonlinear_encoder2 = nonlinear_encoder2(input_layer2)
#     output_encoder1 = output_linear_encoder1+output_nonlinear_encoder1
#     output_encoder2 = output_linear_encoder2+output_nonlinear_encoder2
#     output_encoder = tf.concat([output_encoder1, output_encoder2], 1)
#     output_linear_decoder = linear_decoder(output_encoder)
#     output_nonlinear_decoder = nonlinear_decoder(output_encoder)
#     output_autoencoder = output_linear_decoder+ output_nonlinear_decoder

#     Combined_model = tf.keras.Model(inputs = input_layer, outputs = output_autoencoder, name =  'Combined_autoencoder')
#     return Combined_model

#THis MODEL IS FOR SHMREVIEW  This model is not required in the end
def Combined_model_creation(input_layer, linear_encoder1, linear_encoder2, nonlinear_encoder1, nonlinear_encoder2, linear_decoder, nonlinear_decoder):
    input_layer1, input_layer2 = tf.split(input_layer,[12,14],1)
    output_linear_encoder1 = linear_encoder1(input_layer1)
    output_linear_encoder2 = linear_encoder2(input_layer2)
    output_nonlinear_encoder1 = nonlinear_encoder1(input_layer1)
    output_nonlinear_encoder2 = nonlinear_encoder2(input_layer2)
    output_encoder1 = output_linear_encoder1+output_nonlinear_encoder1
    output_encoder2 = output_linear_encoder2+output_nonlinear_encoder2
    Combined_encoder_model  = tf.keras.Model(inputs = input_layer, outputs = tf.concat([output_encoder1, output_encoder2], 1), name =  'Combined_autoencoder')
    output_encoder = Combined_encoder_model(input_layer)
    output_linear_decoder = linear_decoder(output_encoder)
    output_nonlinear_decoder = nonlinear_decoder(output_encoder)
    output_autoencoder = output_linear_decoder+ output_nonlinear_decoder

    Combined_model = tf.keras.Model(inputs = input_layer, outputs = output_autoencoder, name =  'Combined_autoencoder')
    return Combined_model, Combined_encoder_model