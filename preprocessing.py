# -*- coding: utf-8 -*-
"""
Created on Thu May 21 09:18:33 2020

@author: 109457
"""

#import packages
import os
import numpy as np
import pandas as pd 
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split
from MODULES.PREPROCESSING.preprocessing_tools import import_data, rem_zeros, standardization, rescaling

#######################################################################################################################
#Define functions to generate DAMAGED DATA

def damaged_data_Porto(Xtest):
    Xtest_dam = np.ones(shape = Xtest.shape)
    dam_vec = np.ones(Xtest.shape[1]) 
    #D1 - Damage at M1
    # dam_vec[0] = 0.965823678 #D1: Damage at M1 [sensor left] with only clinometers
    # dam_vec[1] = 1.03417632 #D1: Damage at M1 [sensor right] with only clinometers
    dam_vec[12] = 0.965823678 #D1: Damage at M1 [sensor left] with also frequencies
    dam_vec[13] = 1.034176322 #D1: Damage at M1 [sensor right] with also frequencies 
    # S5i = 0.983947895; S5d = 1.016052105 ; S10i =  0.965823678; S10d = 1.034176322; S20i = 0.92460488; S20d = 1.07539512;
   
    # #D2 - Damage at Middle Deck
    # dam_vec[7] = 1.104294479 #D2: Damage at MiddleDeck [sensor 1] with only clinometers
    # dam_vec[8] = 1.104294479 #D2: Damage at MiddleDeck [sensor 2] with only clinometers   
    # dam_vec[9] = 1.104294479 #D2: Damage at MiddleDeck [sensor 3] with only clinometers   
    # dam_vec[19] = 1.104294479 #D2: Damage at MiddleDeck [sensor 1] with also frequencies
    # dam_vec[20] = 1.104294479 #D2: Damage at MiddleDeck [sensor 2] with also frequencies  
    # dam_vec[21] = 1.104294479 #D2: Damage at MiddleDeck [sensor 3] with also frequencies  
    #S5 = 1.048 ; S10 = 1.086 ; S20 = 1.33 ;
    #Dam D2 --> S5 = 1.003067485; S10 = 1.006134969; S20 = 1.012269939;
    #STRAINS --> S5 = 1.049402648; S10 = 1.104294479; S20 = 1.234662577

    
    #D3 - Damage at M6  
    # dam_vec[6] = 0.963929953 #D3: Damage at M6 [sensor right] with only clinometers
    # dam_vec[18] = 0.963929953 #D3 Damage at M6 [sensor right] with also frequencies
    #S5% = 0.982444823; S10% = 0.963929953;  S20%  = 0.920521857; S30% = 0.868176223;

    #D4 - Damage at arch-pier under M5 
    # dam_vec[4] = 1.002100165 #D4: Damage we see M5 [sensor left] with only clinometers
    # dam_vec[5] = 1.002100165 #D4: Damage we see M5 [sensor right] with only clinometers   
    # dam_vec[16] = 1.002100165 #D4: Damage we see M5 [sensor left] with also frequencies
    # dam_vec[17] = 1.002100165 #D4: Damage we see M5 [sensor right] with also frequencies
    #S5 = 1.002291714; S10 = 1.002100165 ; S20 = 1.010210974; S30 = 1.016878821;
    
    # 
   #Affecting EIGENFREQUENCIES

    #Severity 5%
    # D1 = np.array([0,-0.000826456,-0.000174095,-0.000681245,0,-0.000574328,-0.000298547,0,-6.06575E-05,0,-2.42707E-05,-4.72925E-05])
    # D2 = np.array([0,0,-0.000348189,-0.000340623,0,0,-0.000331719,0,-3.03288E-05,-0.000115187,0,0])
    # D3 = np.array([0,-0.000912803,-0.000174095,-0.000817494,0,-0.000670049,-0.000398063,0,-6.06575E-05,0,-2.42707E-05,-4.72925E-05])
    # D4 = np.array([0,-0.000555083,-8.70474E-05,-0.000681245,0,-0.000143582,-6.63438E-05,0.000346773,-0.001152493,-0.000662328,0.000145624,-2.36463E-05])

    #Severity 10%
    D1 = np.array([0,-0.001726924,-0.000261142,-0.001430615,0,-0.001196516,-0.000597094, 0,-0.000121315,0,-4.85413E-05,-9.4585E-05])
    # D2= np.array([0,0,-0.000609331,-0.000681245,0,0,-0.00069661,0,-3.03288E-05,-0.000287969,0,-2.36463E-05])
    # D3 = np.array([0,-0.001899616,-0.000261142,-0.001634989,0,-0.001435819,-0.000796126,0,-0.000121315,-2.87969E-05,-4.85413E-05,-0.000118231])
    # D4 = np.array([0,-0.001110165,-8.70474E-05,-0.001362491,0,-0.000287164,-0.000298547,0.000346773,-0.00245663,-0.001209468,0.000145624,-4.72925E-05])


    # SEVERITY  = 20%:
    # D1 = np.array([0,-0.003762227,-0.000522284,-0.003133728,0,-0.002584474,-0.001293704,0,-0.00024263,-2.87969E-05,-9.70827E-05,-0.00018917])
    # D2 = np.array([0,0,-0.00130571,-0.001430615,0,-4.78606E-05,-0.001559079,0,-3.03288E-05,-0.000633531,-2.42707E-05,-2.36463E-05])
    # D3 = np.array([0,-0.004144618,-0.000609331,-0.003542476,0,-0.00306308,-0.001724939,0,-0.00024263,-5.75937E-05,-9.70827E-05,-0.000260109])
    # D4 = np.array([0,-0.002368353,-0.000174095,-0.002929355,0,-0.000622188,-0.000829297,0.000346773,-0.005337862,-0.002303749,0.000145624,-7.09388E-05])
    


    dam_vec[0:12] = 1+D1 #Use when frequencies are included
    Xtest_dam = dam_vec*Xtest
    return Xtest_dam

def damaged_data_Mexico(Xtest):
    Xtest_dam = np.ones(shape = Xtest.shape)
    dam_vec = np.ones(Xtest.shape[1])
    dam_vec[0] = 0.5
    Xtest_dam = dam_vec*Xtest
    return Xtest_dam

#############################################################################################################################
#Solve the preprocessing: obtain std datasets for train/val/test/dam_test

def preprocessing_Porto():
    #Import  data
    # path = os.path.join("Data","Data_Porto.npy")
    path = os.path.join("Data","DATA0.npy")
    Data  = np.load(path)
    # Data = Data[8253:25260] #We need only the last 2
    # Data = Data[6087:(Data.shape[0]-0)] #We need only the last 5 
    # Choose the sensors you want to use (some of them have failures or are neglectable)
    # Data =  Data[:,(0,1,8,9,55,56,62,42,43,44,45,46,47,48)] #Without EIGENFREQUENCIES
    # Data =  Data[:,(85,86,87,88,89,90,91,92,93,94,95,96)] #ONLY EIGENFRQUENCIES    
    Data =  Data[:,(85,86,87,88,89,90,91,92,93,94,95,96,0,1,8,9,55,56,62,42,43,44,45,46,47,48)] #With EIGENFRQUENCIES
    #14,15,16,17 extra stresses corresponding to M2 but reducing captured level of info.
    #thermometers: 2,3,63,64
    #Better not consider them because it helps to improve difference damaged-undamaged
    #  Data =  Data[:,(0,1,8,9,55,56,62,42,43,44,45,46,47,48)] #Without EIGENFREQUENCIES

    #Preprocessing
    Data = Data[~np.isnan(Data).any(axis=1)] #Remove nan values from the chosen sensors
    
    Data  = Data[0:(int(np.round(0.9*Data.shape[0]))-0),:] #For TRAINING+VALIDATION
    Xtest = Data[(int(np.round(0.9*Data.shape[0]))+1):Data.shape[0]] #FOR TESTING
    
    train_dim = 0.80;
    Xtrain, Xval = train_test_split(Data, test_size = 1-train_dim, random_state=1234)
    
    #Creae Damaged Testing data
    Xtest_dam = damaged_data_Porto(Xtest)

    del(Data)
    return Xtrain, Xval, Xtest, Xtest_dam

def preprocessing_Mexico():
    #Import  data
    path = os.path.join("Data","Data_Mexico.npy")
    Data  = np.load(path)
    
    #Preprocessing
    Data = rem_zeros(Data) #Remove nan values from the chosen sensors
    Data  = Data[0:(int(np.round(0.9*Data.shape[0]))-0),:] #For TRAINING+VALIDATION
    Xtest = Data[(int(np.round(0.9*Data.shape[0]))+1):Data.shape[0]] #FOR TESTING
    
    train_dim = 0.85;
    Xtrain, Xval = train_test_split(Data, test_size = 1-train_dim, random_state=1234)
    
    #Creae Damaged Testing data
    Xtest_dam = damaged_data_Mexico(Xtest)
    
    del(Data)
    return Xtrain, Xval, Xtest, Xtest_dam


#################################################################################################################################

def preprocessing_interface(filename,bridge_loc,enc_dim):
    if bridge_loc =="PORTO":
       Xtrain, Xval, Xtest, Xtest_dam= preprocessing_Porto()
    elif bridge_loc =="MEXICO":
       Xtrain, Xval, Xtest, Xtest_dam= preprocessing_Mexico()
    
    #standardization of the data (applied to the input data)
    std_model = rescaling(Xtrain,0.5,1.5)
    Xtrain_std = std_model.transform(Xtrain)
    Xval_std = std_model.transform(Xval)
    Xtest_std = std_model.transform(Xtest)
    Xtest_dam_std = std_model.transform(Xtest_dam)
    #################################################################################
    # SOLVE PCA TO OBTAIN PRINCPAL COMPONENTS FOR THE LINEAR AUTOENCODER
    # from sklearn.decomposition import PCA
    # pca = PCA(n_components=enc_dim)
    # pca.fit(Xtrain_std)
    # PCs  = pca.components_
    # np.save(os.path.join("Output","PCs"+str(filename)),PCs)
    return Xtrain_std, Xval_std, Xtest_std, Xtest_dam_std
        


# from matplotlib import pyplot as plt
# Freq_plot  = plt.figure()
# plt.plot(Data[5000:6000,0])
# plt.legend(['$1^{st}$ eigenfrequency', '$2^{nd}$ eigenfrequency'], loc='lower right', fontsize = 14)
# Freq_plot.savefig(os.path.join("Figures","Freq_plot"+str(filename)),dpi = 500, bbox_inches='tight')
# plt.show()

# Inc_plot  = plt.figure()
# plt.plot(Data[:,(14,15)])
# plt.legend(['A', 'B', 'C', 'D', 'E', 'F','G'], loc='lower right', fontsize = 14)
# # Inc_plot.savefig(os.path.join("Figures","Freq_plot"+str(filename)),dpi = 500, bbox_inches='tight')
# plt.show()


D1_plot = plt.figure()
plt.plot(Xtest[:,12],color = 'blue', linewidth = 1.5)
plt.plot(Xtest_dam[:,12], color = 'red', linewidth = 1.5)
plt.xlabel('Observation',fontsize = 14)
plt.ylabel('Inclination (rad)', fontsize = 14)
plt.legend(['$S_{11}$ healthy', ' $S_{11}$ damage D1 10%'], loc  = 'lower right', fontsize = 14  )
D1_plot.savefig(os.path.join("Figures","S11_damage_testdata"),dpi = 500, bbox_inches='tight')
plt.show()

D1_plot_freq = plt.figure()
plt.plot(Xtest[:,3],color = 'blue', linewidth = 1.5)
plt.plot(Xtest_dam[:,3], color = 'red', linewidth = 1.5)
plt.xlabel('Observation',fontsize = 14)
plt.ylabel('Frequency (Hz)', fontsize = 14)
plt.legend(['$f_{4}$ healthy', ' $f_{4}$ damage D1 10%'], loc  = 'lower left', fontsize = 14  )
D1_plot_freq.savefig(os.path.join("Figures","f3_damage_testdata"),dpi = 500, bbox_inches='tight')
plt.show()

