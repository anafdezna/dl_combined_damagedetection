# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 10:52:37 2020

@author: 109457
"""
import numpy as np 
import pandas
import seaborn
import scipy
import sklearn
import os 
import matplotlib
from matplotlib import pyplot as plt


#cumulated damage indicator with k 
def cumulated_errors(train_rec_error, val_rec_error, test_rec_error, test_rec_error_dam, k):
    Train_rec_error = np.zeros(shape = (len(train_rec_error)-k,1))
    for i in range(Train_rec_error.shape[0]):
        Train_rec_error[i,:] =  (np.sum(train_rec_error[i:(k+i),:]))/k
    
    Val_rec_error = np.zeros(shape = (len(val_rec_error)-k,1))
    for i in range(Val_rec_error.shape[0]):
        Val_rec_error[i,:] =  (np.sum(val_rec_error[i:(k+i),:]))/k
    
    Test_rec_error = np.zeros(shape = (len(test_rec_error)-k,1))
    for i in range(Test_rec_error.shape[0]):
        Test_rec_error[i,:] =  (np.sum(test_rec_error[i:(k+i),:]))/k
    
    Test_rec_error_dam= np.zeros(shape = (len(test_rec_error_dam)-k,1))
    for i in range(Test_rec_error_dam.shape[0]):
        Test_rec_error_dam[i,:] =  (np.sum(test_rec_error_dam[i:(k+i),:]))/k
    
    Test_rec_errors = np.concatenate((Test_rec_error,Test_rec_error_dam))
    return Train_rec_error, Val_rec_error, Test_rec_error, Test_rec_error_dam, Test_rec_errors

#Graph configuration function (font sizes)
def plot_configuration():
    plt.rc('font', size = 16)          # controls default text sizes
    plt.rc('axes', titlesize = 16)     # fontsize of the axes title
    plt.rc('axes', labelsize = 16)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize = 16)    # fontsize of the tick labels
    plt.rc('ytick', labelsize = 16)    # fontsize of the tick labels
    plt.rc('legend', fontsize = 16)   # legend fontsize
    plt.rc('figure', titlesize= 16)   # fontsize of the figure title

#Código gráficos
def plot_predicted_values_vs_ground_truth(gt_array, pred_array, title_label, filename):
    # ymin = np.min((gt_array.min(),pred_array.min()))
    # ymax = np.max((gt_array.max(), pred_array.max()))
    #min_val,max_val = ymin,ymax #for adaptive scaling
    min_val,max_val = -3,3 #for common scaling
    increment = max_val - min_val 
    limits = (min_val -0.05*increment, max_val + 0.05*increment)
    
    x,y = pandas.Series(gt_array,name = r'Ground Truth'),pandas.Series(pred_array, name = r'Predicted')
    g = seaborn.jointplot(x=x, y=y, kind = 'hex', color = '#1d6d68', joint_kws = {'gridsize':30,'bins':'log'},xlim=limits, ylim = limits)
    #seaborn.regplot(pandas.Series(np.arange(limits[0],limits[1],0.01)),pandas.Series(np.arange(limits[0],limits[1],0.01)), ax = g.ax_joint, scatter = False)
    g.ax_joint.plot(np.linspace(limits[0], limits[1]), np.linspace(limits[0], limits[1]),'--r', linewidth=4)
    g.set_axis_labels(xlabel = r'Ground truth', ylabel = r'Predicted', fontsize = 18)
    g.fig.suptitle(title_label, y = 0.99)
    #Control the tick & tick labels of the axes 
    #For the ticks (include or not the tick in the axes)
    # g.ax_joint.tick_params(bottom=True, top = True, left = True)
    # For the labels
    # g.ax_joint.set_xticklabels(g.ax_joint.get_xticks(), size= 10)
    # g.ax_joint.set_yticklabels(g.ax_joint.get_yticks(), size= 10)
    # g.ax_joint.tick_params(bottom=False) 
    g.ax_joint.set_xticklabels([])
    g.ax_joint.set_yticklabels([])

    #############################################################################################
    #Introducir el r^2 
    
    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(gt_array,pred_array)
    textstr = '\n'.join((
        # r'$\mathrm{rms} = %.2f$'%(rms,),
        # '#data ='% (numpy.size(gt_array),),
        r'$\mathrm{r^2}=%.4f$'%(r_value**2,),
        # r'$\mathrm{p}=%.2f$'%(p_value,),
        ))
    
    #These are matplotlib.patch.Patch properties
    props = dict(boxstyle = 'round', alpha = 0.5, facecolor = 'none')
    # Place a textbox in upper left in axes coords
    g.ax_joint.text(0.05,0.95, textstr, transform  = g.ax_joint.transAxes, fontsize = 18, verticalalignment = 'top', bbox=props)
    ###########################################################################################
    plt.tight_layout()
    #cbar_ax = g.fig.add_axes([1., 0.1, .075, .75])  # x, y, width, height
    #plt.colorbar(cax=cbar_ax, norm = matplotlib.colors.Normalize(vmin = 1, vmax = 1000))
    #plt.subplots_adjust(left = 0.1, right = 0.8, top = 0.95, bottom = 0.1)
    #g.fig.subplots_adjust(right = 0.8) #control de margenes
    #plt.show()
    # if filename.endswith('.png'):
    g.fig.savefig(os.path.join("Figures","Figs",filename),dpi = 600)
    #     else:
    #g.fig.savefig(filename)
    # plt.close('all')
    # #close the pyplot sesion
    # plt.clf()
    # plt.cla()
    # plt.close()


def plot_crossplots(Xtrain_std,Xtest_std,train_predictions,test_predictions):
    #seaborn.set(style = 'white', font_scale = 1.8)
    for i in range(Xtrain_std.shape[1]):
         plot_predicted_values_vs_ground_truth(Xtrain_std[:,i],train_predictions[:,i],'', 'Crossplot_TrainS'+str(i+1))
    for i in range(Xtest_std.shape[1]):
        plot_predicted_values_vs_ground_truth(Xtest_std[:,i],test_predictions[:,i],'', 'Crossplot_TestS'+str(i+1)) 


def plot_outliers_dam(Train_rec_error, Test_rec_errors, dam_level, percentile):
    x = np.arange(len(Test_rec_errors))
    Lim = np.percentile(Train_rec_error,percentile)
    col = np.where(Test_rec_errors<Lim,'g','r')
    C_chart, ax = plt.subplots()
    plt.axvspan(np.round(0.5*Test_rec_errors.shape[0]), Test_rec_errors.shape[0], facecolor='lightgrey', alpha=0.5, zorder = 0)
    for i in range( Test_rec_errors.shape[0]):
        plt.scatter(x[i], Test_rec_errors[i], s = 2, c=col[i])
    
    plt.ylabel('Damage indicator \u03C1 ')
    plt.xlabel('Measurement')
    text_ypos = Lim + 0.88
    # text_ypos = Lim + 0.135
    # C_chart.text(0.24,text_ypos, '\u03B1 = '+str(round(Lim,2)), color = 'k')
    C_chart.text(0.15,0.8, 'Damage: D1', color = 'black')
    plt.plot(0.006)
    limit = np.ones(len(x))
    plt.plot(Lim*limit, color = 'k', markersize = 8)
    #plt.xlim(right = Test_rec_errors.shape[0]) #control the end of grey shadow
    C_chart.savefig(os.path.join("Figures","Controlchart_D"+str(np.int(100*dam_level))+".png"), dpi = 500, bbox_inches='tight')
    plt.show()


def plot_loss_evolution(history,filename):
    loss_plot  = plt.figure()
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    plt.plot(loss,color = '#072FD1',)
    plt.plot(val_loss, color = 'red')
    #plt.title('model loss')
    plt.ylabel(r'$\mathcal{L}_{\theta^{*}}$')
    #plt.ylabel(r'$s(t) = \mathcal{A}')
    plt.xlabel('Epoch')
    xmin, xmax = plt.xlim()
    # ymin, ymax = plt.ylim()
    ymin,ymax = 0.0001, 0.01
    scale_factor = 1
    plt.yscale('log')
    plt.xlim(xmin *scale_factor, xmax * scale_factor)
    plt.ylim(ymin * scale_factor, ymax * scale_factor)
    plt.legend(['Train', 'Validation'], loc='upper right', fontsize = 18)
    loss_plot.savefig(os.path.join("Figures","Model_loss"+str(filename)),dpi = 500, bbox_inches='tight')
    plt.show()
    
    
    
def plot_losses(history,filename):
    loss_plot  = plt.figure()
    loss = history.history['loss']
    loss_Global = history.history['loss_Global']
    loss_Local = history.history['loss_Local']
    loss_sum = loss_Global+loss_Local
    plt.plot(loss_Global, color = '#072FD1')
    plt.plot(loss_Local, color = 'black')
    plt.plot(loss,color = 'red',)
    # plt.plot(loss_sum, color = 'green')
    #plt.title('model loss')
    plt.ylabel(r'$\mathcal{L}_{\theta^{*}}$')
    #plt.ylabel(r'$s(t) = \mathcal{A}')
    plt.xlabel('Epoch')
    xmin, xmax = plt.xlim()
    ymin,ymax = 0.0001, 0.1
    scale_factor = 1
    plt.yscale('log')
    plt.xlim(xmin *scale_factor, xmax * scale_factor)
    plt.ylim(ymin * scale_factor, ymax * scale_factor)
    plt.legend(['Global', 'Local', 'Total'], loc='upper right', fontsize = 18)
    loss_plot.savefig(os.path.join("Figures","Model_loss"+str(filename)),dpi = 500, bbox_inches='tight')
    plt.show()


def plot_histogram(Train_rec_error, Lim, percentile):
    Histo = plt.figure()
    plt.hist(Train_rec_error, bins = 16, color = 'skyblue', alpha= 0.5, histtype='bar', ec='black')
    plt.axvline(x = Lim, ymax = 0.55, color = "red",linestyle='--', linewidth = '3')
    plt.axvline(x = Lim, ymin = 0.7, color = "red",linestyle='--', linewidth = '3')
    plt.xlabel('Reconstruction error')
    plt.ylabel('Frequency')
    Histo.text(0.17, 0.58, 'p-99 = '+str(round(Lim,2)), color = 'red')
    Histo.savefig(os.path.join("Figures","Figs","Train_hist_Porto"),dpi = 500, bbox_inches='tight')
    plt.show()
    return Lim

def calculate_errors(Xstd, predictions):
    rec_error = np.zeros(shape = [ Xstd.shape[0],1])
    for i in range(Xstd.shape[0]):
        rec_error[i] = 1/(Xstd.shape[1])*np.sum((Xstd[i,:] - predictions[i,:])**2)
    return rec_error


def calculate_metrics(Test_rec_error, Test_rec_error_dam, Lim):
    FP = len(np.where(Test_rec_error > Lim)[0])
    FN = len(np.where(Test_rec_error_dam < Lim)[0])
    TP = len(np.where(Test_rec_error_dam>Lim)[0])
    TN = len(np.where(Test_rec_error<Lim)[0])
    # accuracy = (TP+TN)/(2*len(Test_rec_error))*100
    # precision = TP/(TP+FP)*100
    # recall = TP/(TP+FN)*100
    # f1_score = 2*(precision*recall)/(precision + recall)
    print(FP,FN,TP,TN)
    return FP,FN,TP,TN


def calculate_ROC_metrics(Test_rec_error, Test_rec_error_dam, Train_rec_error,filename):
    percentiles = np.arange(0, 100, 1)
    FP_rates = np.zeros(len(percentiles))
    TP_rates = np.zeros(len(percentiles))
    FN_rates = np.zeros(len(percentiles))
    TN_rates = np.zeros(len(percentiles))
    Lims = np.percentile(Test_rec_error,percentiles)
    for i in range(0, len(percentiles)):
        FP_rates[i] = len(np.where(Test_rec_error > Lims[i])[0])/Test_rec_error.shape[0]
        TP_rates[i] = len(np.where(Test_rec_error_dam > Lims[i])[0])/Test_rec_error.shape[0]
        FN_rates[i] = len(np.where(Test_rec_error_dam < Lims[i])[0])/Test_rec_error.shape[0]
        TN_rates[i] = len(np.where(Test_rec_error < Lims[i])[0])/Test_rec_error.shape[0]
    plt.plot(FP_rates,TP_rates, color  = 'red', linewidth = 2.5)
    plt.plot(0,0)
    plt.legend([str(filename)],loc='lower right', fontsize = 14)
    plt.xlabel(r'False Positive Rate')
    plt.ylabel(r'True Positive Rate')
    
    # plt.figure()
    return FP_rates, TP_rates, Lims


def plot_ROCs(FP_rates_freqs,FP_rates_static, FP_rates_combined, TP_rates_freqs, TP_rates_static, TP_rates_combined,filename):
    ROCs_plot = plt.figure()
    plt.plot(FP_rates_static,TP_rates_static, color  = 'limegreen', linewidth = 2.5, marker  = '^' , markersize  = 4)
    plt.plot(FP_rates_freqs,TP_rates_freqs, color  = 'blue', linewidth = 2.5)
    plt.plot(FP_rates_combined, TP_rates_combined, color = "red",  linestyle='--', linewidth = 2.5)
    plt.plot(FP_rates_combined, FP_rates_combined, color = "grey", linestyle=':', linewidth = 2.5)
    # y0 = np.arange(0, 1.1, 0.1)
    # x0 = np.zeros(len(y0))
    # plt.plot(x0,y0, color = 'blue', linewidth = 2.5) #Activate when Static reaches perfect classification
    # plt.plot(x0,y0, color = 'red',linestyle = '--', linewidth = 2.5) #Activate when Combined reaches perfect classification
    plt.legend(['Local variables', 'Global variables', 'Local + Global variables'],loc='lower right', fontsize = 14)
    # plt.legend(['Severity 10% ', 'Severity 15%', 'Severity 20%'],loc='lower right', fontsize = 14)
    plt.xlabel(r'False Positive Rate')
    plt.ylabel(r'True Positive Rate')
    ROCs_plot.savefig(os.path.join("Figures","Review_ROCs","ROC_curves_"+str(filename)),dpi = 500, bbox_inches='tight')
    plt.show()



def plot_ROCs_b(FP_rates_D1,FP_rates_D2, FP_rates_D3, FP_rates_D4, TP_rates_D1, TP_rates_D2, TP_rates_D3, TP_rates_D4, filename):
    ROCs_plot = plt.figure()
    plt.plot(FP_rates_D1,TP_rates_D1, color  = 'red', linewidth = 2.5)
    plt.plot(FP_rates_D2,TP_rates_D2, color  = 'blue', linewidth = 2.5)
    plt.plot(FP_rates_D3, TP_rates_D3, color = "limegreen", linewidth = 2.5)
    plt.plot(FP_rates_D4, TP_rates_D4, color = "#FFA500", linewidth = 2.5)
    plt.plot(FP_rates_D1, FP_rates_D1, color = "grey", linestyle=':', linewidth = 1.5)
    y0 = np.arange(0,1.1,0.1)
    x0 = np.zeros(len(y0))
    # plt.plot(x0,y0, color = 'blue', linewidth = 2.5) #Activate when Static reaches perfect classification
    # plt.plot(x0,y0, color = 'red',linestyle = '--', linewidth = 2.5) #Activate when Combined reaches perfect classification
    plt.legend(['D1', 'D2', 'D3', 'D4'],loc='lower right', fontsize = 14)
    # plt.legend(['Severity 10% ', 'Severity 15%', 'Severity 20%'],loc='lower right', fontsize = 14)
    plt.xlabel(r'False Positive Rate')
    plt.ylabel(r'True Positive Rate')
    ROCs_plot.savefig(os.path.join("Figures","ROCs","ROC_curves_"+str(filename)),dpi = 500, bbox_inches='tight')
    plt.show()
    

    
def plot_outliers_dam2(Train_rec_error,Test_rec_error, Test_rec_errorD1,Test_rec_errorD2,Test_rec_errorD3,Test_rec_errorD4, dam_level, percentile):
    Test_rec_errors = np.concatenate((Test_rec_error, Test_rec_errorD1, Test_rec_errorD2, Test_rec_errorD3, Test_rec_errorD4), axis = 0)
    Test_rec_errors = np.log(Test_rec_errors)
    x = np.arange(len(Test_rec_errors))
    Lim = np.percentile(np.log(Test_rec_error),percentile)
    col = np.where(Test_rec_errors<Lim,'g','r')
    C_chart, ax = plt.subplots()
    plt.axvspan(np.round(0.20*Test_rec_errors.shape[0]), Test_rec_errors.shape[0], facecolor='lightgrey', alpha=0.5, zorder = 0)
    ymin = np.min(Test_rec_errors)-0.2
    ymax = np.max(Test_rec_errors)+0.2
    plt.vlines(np.round(0.20*Test_rec_errors.shape[0]), ymin, ymax, colors='k', linewidth = 1, linestyles='--')
    plt.vlines(np.round(0.40*Test_rec_errors.shape[0]), ymin, ymax, colors='k', linewidth = 1, linestyles='--')
    plt.vlines(np.round(0.60*Test_rec_errors.shape[0]), ymin, ymax, colors='k', linewidth = 1, linestyles='--')
    plt.vlines(np.round(0.80*Test_rec_errors.shape[0]), ymin, ymax, colors='k', linewidth = 1, linestyles='--')

    for i in range( Test_rec_errors.shape[0]):
        plt.scatter(x[i], Test_rec_errors[i], s = 2, c=col[i])
    
    plt.ylabel('$log_{10}$(\u03C1)', fontsize = 14)
    plt.xlabel('Measurement', fontsize = 14)
    text_ypos = Lim + 0.88
    # text_ypos = Lim + 0.135
    # C_chart.text(0.24,text_ypos, '\u03B1 = '+str(round(Lim,2)), color = 'k')
    C_chart.text(0.14,0.6, 'Healthy', color = 'black', fontsize = 14)
    C_chart.text(0.34,0.6, 'D1', color = 'black', fontsize = 14)
    C_chart.text(0.48,0.6, 'D2', color = 'black', fontsize = 14)
    C_chart.text(0.62,0.6, 'D3', color = 'black',fontsize = 14)
    C_chart.text(0.76,0.6, 'D4', color = 'black', fontsize = 14)
    # plt.plot(0.006)
    limit = np.ones(len(x))
    plt.plot(Lim*limit, color = 'blue',linewidth = 2.5, markersize = 8)
    #plt.xlim(right = Test_rec_errors.shape[0]) #control the end of grey shadow
    C_chart.savefig(os.path.join("Figures","Controlchart_D"+str(np.int(100*dam_level))+".png"), dpi = 500, bbox_inches='tight')
    plt.show()


def compress_Z_space(Zvec):
    from sklearn.decomposition import PCA
    pca = PCA(n_components = 2)
    pca.fit(Zvec)
    PCs = pca.components_
    Z_pca = pca.transform(Zvec)
    return Z_pca

def plot_Z_space(Ztrain, Ztest, Ztest_dam):
    from sklearn.decomposition import PCA
    pca = PCA(n_components = 2)
    pca.fit(Ztrain)
    PCs = pca.components_
    Z_pca_train = pca.transform(Ztrain)
    Z_pca_test = pca.transform(Ztest)
    Z_pca_test_Dam = pca.transform(Ztest_dam)
    Zspace_plot = plt.figure()
    plt.plot(Z_pca_train[:,0],Z_pca_train[:,1], 'o', markersize = 1, color = 'blue')
    plt.plot(Z_pca_test[:,0],Z_pca_test[:,1], 'o', markersize = 1, color = 'green')
    plt.plot(Z_pca_test_Dam[:,0],Z_pca_test_Dam[:,1], 'o', markersize = 1, color = 'red')
    plt.show()
    

def plot_latent_vars(Z_test, Z_test_dam, filename):
    x = np.linspace(1,Z_test.shape[0], Z_test.shape[0])
    plt.figure(figsize = (10,10))
    ax1 = plt.subplot(811)
    plt.plot(x,Z_test[:,0],color = 'blue', marker = 'o', markersize = 1)
    plt.plot(x,Z_test_dam[:,0],color = 'red', marker = 'x', markersize = 1)
    # plt.plot(x,(Z_test[:,0]-Z_test_dam[:,0])**2,color = 'blue')
    # plt.plot(np.zeros(shape = (Z_test.shape[0],1)), color = 'white')
    plt.legend(['Undamaged', 'Damaged'], loc = 'upper right')
    ax1.set_ylabel(r'$z_{1}$', rotation = 'horizontal',fontsize = 16)
    ax1.get_yaxis().set_label_coords(-0.1,0.4)
    # make these tick labels invisible
    plt.tick_params(axis = 'y',labelsize = 16)
    plt.tick_params('x', labelbottom=False)
    ax2 = plt.subplot(812, sharex=ax1)
    plt.plot(x,Z_test[:,1],color = 'blue', marker = 'o', markersize = 1)
    plt.plot(x,Z_test_dam[:,1],color = 'red', marker = 'x', markersize = 1)
    # plt.plot(x,(Z_test[:,1]-Z_test_dam[:,1])**2, color = 'blue')
    # plt.plot(np.zeros(shape = (Z_test.shape[0],1)), color = 'white')
    ax2.set_ylabel(r'$z_{2}$', rotation = 'horizontal',fontsize = 16)
    ax2.get_yaxis().set_label_coords(-0.1,0.4)
    plt.tick_params(axis = 'y',labelsize = 16)
    plt.tick_params('x', labelbottom=False)
    ax3 = plt.subplot(813, sharex=ax1)
    plt.plot(x,Z_test[:,2],color = 'blue', marker = 'o', markersize = 1)
    plt.plot(x,Z_test_dam[:,2],color = 'red', marker = 'x', markersize = 1)
    ax3.set_ylabel(r'$z_{3}$', rotation = 'horizontal',fontsize = 16)
    ax3.get_yaxis().set_label_coords(-0.1,0.4)
    # plt.plot(x,(Z_test[:,2]-Z_test_dam[:,2])**1, color = 'blue')
    # plt.plot(np.zeros(shape = (Z_test.shape[0],1)), color = 'white')
    plt.tick_params(axis = 'y',labelsize = 16)
    plt.tick_params('x', labelbottom=False)
    ax4 = plt.subplot(814, sharex=ax1)
    plt.plot(x,Z_test[:,3],color = 'blue', marker = 'o', markersize = 1)
    plt.plot(x,Z_test_dam[:,3],color = 'red', marker = 'x', markersize = 1)
    ax4.set_ylabel(r'$z_{4}$', rotation = 'horizontal',fontsize = 16)
    ax4.get_yaxis().set_label_coords(-0.11,0.4)
    # plt.plot(x,(Z_test[:,3]-Z_test_dam[:,3])**2, color = 'blue')
    # plt.plot(np.zeros(shape = (Z_test.shape[0],1)), color = 'white')
    plt.tick_params(axis = 'y',labelsize = 16)
    plt.tick_params('x', labelbottom=False)
    ax5 = plt.subplot(815, sharex=ax1)
    plt.plot(x,Z_test[:,4],color = 'blue', marker = 'o', markersize = 1)
    plt.plot(x,Z_test_dam[:,4],color = 'red', marker = 'x', markersize = 1)
    ax5.set_ylabel(r'$z_{5}$', rotation = 'horizontal',fontsize = 16)
    ax5.get_yaxis().set_label_coords(-0.1,0.4)
    # plt.plot(x,(Z_test[:,4]-Z_test_dam[:,4])**2, color = 'blue')
    # plt.plot(np.zeros(shape = (Z_test.shape[0],1)), color = 'white')
    plt.tick_params(axis = 'y',labelsize = 16)
    plt.tick_params('x', labelbottom=False)
    ax6= plt.subplot(816, sharex=ax1)
    plt.plot(x,Z_test[:,5],color = 'blue', marker = 'o', markersize = 1)
    plt.plot(x,Z_test_dam[:,5],color = 'red', marker = 'x', markersize = 1)
    ax6.set_ylabel(r'$z_{6}$', rotation = 'horizontal',fontsize = 16)
    ax6.get_yaxis().set_label_coords(-0.1,0.4)
    # plt.plot(x,(Z_test[:,5]-Z_test_dam[:,5])**2, color = 'blue')
    # plt.plot(np.zeros(shape = (Z_test.shape[0],1)), color = 'white')
    plt.tick_params(axis = 'y',labelsize = 16)
    plt.tick_params('x', labelbottom=False)
    ax7 = plt.subplot(817, sharex=ax1)
    plt.plot(x,Z_test[:,6],color = 'blue', marker = 'o', markersize = 1)
    plt.plot(x,Z_test_dam[:,6],color = 'red', marker = 'x', markersize = 1)
    ax7.set_ylabel(r'$z_{7}$', rotation = 'horizontal',fontsize = 16)
    ax7.get_yaxis().set_label_coords(-0.1,0.4)
    # plt.plot(x,(Z_test[:,6]-Z_test_dam[:,6])**2, color = 'blue')
    # plt.plot(np.zeros(shape = (Z_test.shape[0],1)), color = 'white')
    plt.tick_params(axis = 'y',labelsize = 16)
    plt.tick_params('x', labelbottom=False)
    ax8 = plt.subplot(818, sharex=ax1)
    plt.plot(x,Z_test[:,7],color = 'blue', marker = 'o', markersize = 1)
    plt.plot(x,Z_test_dam[:,7],color = 'red', marker = 'x', markersize = 1)
    ax8.set_ylabel(r'$z_{8}$', rotation = 'horizontal',fontsize = 16)
    ax8.get_yaxis().set_label_coords(-0.1,0.4)
    # plt.plot(x,(Z_test[:,7]-Z_test_dam[:,7])**2, color = 'blue')
    # plt.plot(np.zeros(shape = (Z_test.shape[0],1)), color = 'white')
    plt.tick_params(axis = 'y',labelsize = 16)
    plt.tick_params('x', labelsize=16)
    plt.xlabel('Observation', fontsize = 16)
    # plt.ylabel('Squared error')
    plt.savefig(os.path.join("Figures","Latent_rep", "Latent_Variables"+ filename +".png"), dpi = 500, bbox_inches='tight')
    plt.show()
    
    
def plot_latent_vars3(Z_test, Z_test_dam, filename):
    x = np.linspace(1,Z_test.shape[0], Z_test.shape[0])
    fig = plt.figure(figsize = (10,5))
    ax1 = plt.subplot(311)
    plt.plot(x,Z_test[:,0],color = 'blue', marker = 'o', markersize = 1)
    plt.plot(x,Z_test_dam[:,0],color = 'red', marker = 'x', markersize = 1)
    ax1.set_ylabel(r'$z_{1}$', rotation = 'horizontal',fontsize = 16)
    ax1.get_yaxis().set_label_coords(-0.12,0.4)
    # plt.plot(x,(Z_test[:,0]-Z_test_dam[:,0])**2,color = 'blue')
    # plt.plot(np.zeros(shape = (Z_test.shape[0],1)), color = 'white')
    plt.legend(['Undamaged', 'Damaged'], loc = 'upper right')
    # make these tick labels invisible
    plt.tick_params(axis = 'y',labelsize = 16)
    plt.tick_params('x', labelbottom=False)
    ax2 = plt.subplot(312, sharex=ax1)
    plt.plot(x,Z_test[:,1],color = 'blue', marker = 'o', markersize = 1)
    plt.plot(x,Z_test_dam[:,1],color = 'red', marker = 'x', markersize = 1)
    ax2.set_ylabel(r'$z_{2}$', rotation = 'horizontal',fontsize = 16)
    ax2.get_yaxis().set_label_coords(-0.12,0.4)
    # plt.plot(x,(Z_test[:,1]-Z_test_dam[:,1])**2, color = 'blue')
    # plt.plot(np.zeros(shape = (Z_test.shape[0],1)), color = 'white')
    plt.tick_params(axis = 'y',labelsize = 16)
    plt.tick_params('x', labelbottom=False)
    ax3 = plt.subplot(313, sharex=ax1)
    plt.plot(x,Z_test[:,2],color = 'blue', marker = 'o', markersize = 1)
    plt.plot(x,Z_test_dam[:,2],color = 'red', marker = 'x', markersize = 1)
    # plt.plot(x,(Z_test[:,7]-Z_test_dam[:,7])**2, color = 'blue')
    # plt.plot(np.zeros(shape = (Z_test.shape[0],1)), color = 'white')
    plt.tick_params(axis = 'y',labelsize = 16)
    plt.tick_params('x', labelsize=16)
    plt.xlabel('Observation',fontsize = 16)
    ax3.set_ylabel(r'$z_{3}$', rotation = 'horizontal',fontsize = 16)
    ax3.get_yaxis().set_label_coords(-0.12,0.4)
    # plt.ylabel('Squared error')
    fig.savefig(os.path.join("Figures","Latent_rep", "Latent_Variables"+ filename +".png"), dpi = 500, bbox_inches='tight')
    plt.show()
#Simple plotsof Data with Labels
# plt.plot(Data[:,0],'red')
# plt.plot(Data[:,1],'blue')
# plt.plot(Data[:,2],'green')
# plt.plot(Data[:,3],'orange')
# plt.legend(['Sensor 1', 'Sensor 2', 'Sensor 3', 'Sensor 4'], loc='upper left', fontsize = 14)
# plt.xlabel('Measurement')
# plt.ylabel("mm")

# #Plot data samples
# dplt = plt.figure()
# plt.plot(Data[:,(8,9,10)])
# plt.xlabel('Measurement')
# plt.ylabel('Strain (MPa)')
# plt.plot(30000,-380)
# plt.title('Measurements for strain gauges at cross-section C')
# plt.legend(['Strain gauge 1', 'Strain gauge 2', 'Strain gauge 3'], loc='lower left', fontsize = 14)
# dplt.savefig(os.path.join("Figures","Data_plot"),dpi = 500, bbox_inches='tight')
# plt.show()

# #Plot data samples
# dplt = plt.figure()
# plt.plot(Data)
# plt.xlabel('Measurement')
# plt.ylabel('Displacement (mm)')
# plt.plot(30000, -0.015)
# plt.legend(['Sensor 1', 'Sensor 2', 'Sensor 3', 'Sensor 4'], loc='upper left', fontsize = 14)
# dplt.savefig(os.path.join("Figures","Data_plot"),dpi = 500, bbox_inches='tight')
# plt.show()


#Simple plots of Freqs with Labels
# freqs_plt = plt.figure()
# plt.plot(Data[:,0])
# plt.plot(Data[:,1])
# plt.plot(Data[:,2])
# plt.plot(Data[:,3])
# plt.plot(Data[:,4])
# plt.plot(Data[:,5])
# plt.plot(Data[:,6])
# plt.plot(Data[:,7])
# plt.plot(Data[:,8])
# plt.plot(Data[:,9])
# plt.plot(Data[:,10])
# plt.plot(Data[:,11])
# # plt.legend(['Sensor 1', 'Sensor 2', 'Sensor 3', 'Sensor 4'], loc='upper left', fontsize = 14)
# plt.xlabel('Measurement')
# plt.ylabel("Frequency (Hz)")
# freqs_plt.savefig(os.path.join("Figures","Freqs_plot"),dpi = 500, bbox_inches='tight')
# plt.show()

# #Simple plots of Inclinometers with Labels
# inclins_plt = plt.figure()
# plt.plot(Data[:,12], color = 'darkblue')
# plt.plot(Data[:,13], color = 'blue')
# plt.plot(Data[:,14], color = 'darkgreen')
# plt.plot(Data[:,15], color = 'green')
# plt.plot(Data[:,16], color = 'red')
# plt.plot(Data[:,17], color = 'orangered')
# plt.plot(Data[:,18], color = 'violet')
# # inclins_plt.text(0.25,0.75, '$S_{51}$ and $S_{52}$')
# plt.legend(['$S_{11}$', '$S_{12}$', '$S_{21}$', '$S_{22}$', '$S_{51}$', '$S_{52}$','$S_{62}$'], ncol = 1, loc='best', fontsize = 14)
# plt.xlabel('Measurement')
# plt.ylabel("Inclination (rad)")
# inclins_plt.savefig(os.path.join("Figures","Inclins_plot_colors"),dpi = 500, bbox_inches='tight')
# plt.show()


# #Simple plots of Inclinometers with Labels
# inclins_plt = plt.figure()
# plt.plot(Data[:,12], color = 'blue')
# plt.plot(Data[:,14], color = 'orangered')
# plt.plot(Data[:,16], color = 'limegreen')
# plt.plot(Data[:,18], color = 'violet')
# plt.plot(Data[:,13], color = 'blue')
# plt.plot(Data[:,15], color = 'orangered')
# plt.plot(Data[:,17], color = 'limegreen')
# # inclins_plt.text(0.25,0.75, '$S_{51}$ and $S_{52}$')
# plt.legend(['$S_{1}$', '$S_{2}$', '$S_{5}$', '$S_{6}$'], ncol = 4, loc='best', fontsize = 14)
# plt.xlabel('Measurement')
# plt.ylabel("Inclination (rad)")
# inclins_plt.savefig(os.path.join("Figures","Inclins_plot"),dpi = 500, bbox_inches='tight')
# plt.show()


# #Simple plots of Strains with Labels
# strains_plt = plt.figure()
# plt.plot(Data[:,19], color = 'darkblue')
# plt.plot(Data[:,20], color = 'blue')
# plt.plot(Data[:,21], color = 'royalblue')
# plt.plot(Data[:,22], color = 'red')
# plt.plot(Data[:,23], color  = 'orangered' ) 
# plt.plot(Data[:,24], color = 'darkorange')
# plt.plot(Data[:,25], color = 'gold')
# plt.legend(['$S_{31}$', '$S_{32}$', '$S_{33}$', '$S_{41}$', '$S_{42}$', '$S_{43}$','$S_{44}$'],ncol =1, loc='best', fontsize = 14)
# plt.xlabel('Measurement')
# plt.ylabel("Strain (MPa)")
# strains_plt.savefig(os.path.join("Figures","Strains_plot_colors"),dpi = 500, bbox_inches='tight')
# plt.show()



# #Simple plots of Strains with Labels
# strains_plt = plt.figure()
# plt.plot(Data[:,19], color = 'royalblue')
# plt.plot(Data[:,22], color = 'red')
# plt.plot(Data[:,20], color = 'royalblue')
# plt.plot(Data[:,21], color = 'royalblue')
# plt.plot(Data[:,23], color  = 'red' ) 
# plt.plot(Data[:,24], color = 'red')
# plt.plot(Data[:,25], color = 'red')
# plt.legend(['$S_{3}$', '$S_{4}$'],ncol =2, loc='best', fontsize = 14)
# plt.xlabel('Measurement')
# plt.ylabel("Stress (MPa)")
# strains_plt.savefig(os.path.join("Figures","Strains_plot"),dpi = 500, bbox_inches='tight')
# plt.show()


