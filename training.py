# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 17:46:07 2020

@author: 110137
"""

import tensorflow as tf
from MODULES.MODEL.submodels_creation import submodels
from MODULES.MODEL.model_creation import PCA_model_creation, ResNet_model_creation, Combined_model_creation
from MODULES.POSTPROCESSING.postprocessing import plot_loss_evolution
import numpy as np
from tensorflow.keras import backend as K
import os
from tensorflow.keras.callbacks import ModelCheckpoint

###########################################################################################
 
class training_info_initializer():
    #definicion e inicialización de las variables que van dentro 
    def __init__(self, n_epoch = 20, batch_size = 512, LR = 1e-03, metrics = "accuracy", loss = "mean_squared_error", shuffle = True,):        
        self.n_epoch = n_epoch
        self.Batch_size = batch_size
        self.LR = LR
        self.Metrics = metrics
        self.Loss = loss
        self.Shuffle = shuffle
        if n_epoch == None or batch_size == None or LR == None or loss == None:
            print("************************************************************************")
            print("Please initialize the info for TRAINING!")
            print("************************************************************************")
            quit()



def building_models(Residual, PCA, arch_info, train_info, filename):
    # Create the submodels based on the previos architectures. We define the encoder and decoder submodels
    input_layer, linear_encoder, linear_decoder, nonlinear_encoder, nonlinear_decoder = submodels(arch_info)
    model_parts  = [input_layer, linear_encoder, linear_decoder, nonlinear_encoder, nonlinear_decoder]
    models = [None, None,None]
    #We create the final model of the AUTOENCODER
    #list  para guardar los modelos. Inicializamos
    # models = [None,None]
    # histories = [None,None]
    # filepath = os.path.join('Output','best_model'+str(filename)+'.hdf5')
    # checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='auto')
    if Residual:
        Residual_autoencoder, Residual_encoder = ResNet_model_creation(input_layer,linear_encoder,linear_decoder, nonlinear_encoder, nonlinear_decoder)
        Residual_autoencoder.compile(metrics=[train_info.Metrics], loss= train_info.Loss, optimizer = tf.keras.optimizers.Adamax(learning_rate = train_info.LR)) #Define the optimizer (SGD, RMSprop, Adam, adaline*) 
        Residual_autoencoder.summary()
        models[1] = Residual_autoencoder
        models[2] = Residual_encoder
    if PCA:
        PCA_autoencoder = PCA_model_creation(input_layer,linear_encoder,linear_decoder, nonlinear_encoder, nonlinear_decoder)
        PCA_autoencoder.compile(metrics=[train_info.Metrics], loss= train_info.Loss, optimizer = tf.keras.optimizers.Adam(learning_rate = train_info.LR)) #Define the optimizer (SGD, RMSprop, Adam, adaline*) 
        PCA_autoencoder.summary()
        models[0] = PCA_autoencoder
    return models,model_parts



def training_models(model, train_info,Xtrain_std,Xval_std,filename):
    filepath = os.path.join('Output','best_model'+str(filename)+'.hdf5')
    checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='auto')
    history = model.fit(Xtrain_std, Xtrain_std, epochs = train_info.n_epoch, batch_size = train_info.Batch_size, callbacks = checkpoint, shuffle = train_info.Shuffle, validation_data = (Xval_std, Xval_std))
    plot_loss_evolution(history,filename)
    return history



#OLD TRAINING FUNCTION (DEMASIADO COMPACTA)
# def training_models(Residual, PCA, arch_info, train_info, Xtrain_std, Xval_std,filename):

#     # Create the submodels based on the previos architectures. We define the encoder and decoder submodels
#     input_layer, linear_encoder, linear_decoder, nonlinear_encoder, nonlinear_decoder = submodels(arch_info)
    
    
#     #We create the final model of the AUTOENCODER
#     #list  para guardar los modelos. Inicializamos
#     models = [None,None]
#     histories = [None,None]
    
#     if Residual:
#         filepath = os.path.join('Output','best_model'+str(filename)+'.hdf5')
#         checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='auto')
#         Residual_autoencoder = ResNet_model_creation(input_layer,linear_encoder,linear_decoder, nonlinear_encoder, nonlinear_decoder)
#         Residual_autoencoder.compile(metrics=[train_info.Metrics], loss= train_info.Loss, optimizer = tf.keras.optimizers.Adam(learning_rate = train_info.LR)) #Define the optimizer (SGD, RMSprop, Adam, adaline*) 
#         Residual_autoencoder.summary()
#         Residual_history = Residual_autoencoder.fit(Xtrain_std, Xtrain_std, epochs = train_info.n_epoch, batch_size = train_info.Batch_size, callbacks = checkpoint, shuffle = train_info.Shuffle, validation_data = (Xval_std, Xval_std))
#         plot_loss_evolution(Residual_history,filename)
#         models[1] = Residual_autoencoder
#         histories[1] = Residual_history
    
#     if PCA:
#         PCA_autoencoder = PCA_model_creation(input_layer, linear_encoder, linear_decoder)
#         PCA_autoencoder.compile(metrics = [train_info.Metrics], loss = train_info.Loss, optimizer = tf.keras.optimizers.RMSprop(learning_rate = train_info.LR))#Define the optimizer (SGD, RMSprop, Adam, adaline*) 
#         PCA_autoencoder.summary()
#         PCA_history = PCA_autoencoder.fit(Xtrain_std, Xtrain_std, epochs = train_info.n_epoch, batch_size = train_info.Batch_size, shuffle = train_info.Shuffle, validation_data = (Xval_std, Xval_std))
#         plot_loss_evolution(PCA_history,filename)
#         models[0] = PCA_autoencoder
#         histories[0] = PCA_history

#     return models, histories



# CUSTOM LOSSES DEFINITION 


def custom_loss(y_actual,y_pred):
    loss = tf.math.reduce_mean(tf.math.square(y_actual - y_pred), axis= None)
    return loss


def loss_Global(y_actual, y_pred):
    aux_var = tf.math.square(y_actual - y_pred)
    loss_Global = tf.math.reduce_mean(aux_var[:,0:12], axis = None)
    return loss_Global*12/26


def loss_Local(y_actual, y_pred):
    aux_var = tf.math.square(y_actual - y_pred)
    loss_Local = tf.math.reduce_mean(aux_var[:,12:y_pred.shape[1]], axis = None)
    return loss_Local*14/26

def tricky_loss(y_actual, y_pred):
    return y_pred




def orthogonality_loss(y_actual,y_pred,W_e,W_d):
    Me = np.dot(W_e,W_e.T)
    Md = np.dpt(W_d, W_d.T)
    I = np.identity(Me.shape[0])
    
    loss_pred = tf.math.reduce_mean(tf.math.square(y_actual - y_pred), axis= None)
    loss_We = K.sqrt(K.sum(K.square(Me-I)))
    loss_Wd =  K.sqrt(K.sum(K.square(Md-I)))
    loss_orthogonality = loss_We + loss_Wd
    loss = loss_pred + loss_orthogonality
                 
    return loss